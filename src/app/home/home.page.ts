import { Component, SecurityContext } from '@angular/core';
import { createWorker } from 'tesseract.js';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import { DomSanitizer } from '@angular/platform-browser';
//import { Buffer } from 'buffer';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  worker: Tesseract.Worker;
  workerReady = false;
  langImage = 'por';
  langSpeech = 'pt-BR';
  speechRate = 1;
  image = 'https://tesseract.projectnaptha.com/img/eng_bw.png';
  buffer: Buffer | undefined;
  ocrResult = '';
  captureProgress = 0;

  constructor(
    private camera: Camera
    ,private file: File
    ,private webview: WebView
    ,private sanitizer: DomSanitizer
    ,private textToSpeech: TextToSpeech
  ) {
    this.loadWorker();
  }

  async loadWorker() {
      this.worker = createWorker({
        logger: progress => {
          //console.log(progress);
          if (progress.status == 'recognizing text'){
            this.captureProgress = parseInt('' + progress.progress * 100);
          }
        }
      });

      await this.worker.load();
      await this.worker.loadLanguage(this.langImage);
      await this.worker.initialize(this.langImage)
      //console.log('END');
      this.workerReady = true;
  }

  ngOnInit() {}

  async captureImage(){

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
      ,correctOrientation: true
      //,allowEdit: true
    }
  
    const tempImage = await this.camera.getPicture(options);
    //console.log("tempImage " + tempImage);

    //mount local file
    const tempFilename = tempImage.substr(tempImage.lastIndexOf('/') + 1);
    const tempBaseFilesystemPath = tempImage.substr(0, tempImage.lastIndexOf('/') + 1);
    const newBaseFilesystemPath = this.file.dataDirectory;
    await this.file.copyFile(tempBaseFilesystemPath, tempFilename, 
                             newBaseFilesystemPath, tempFilename);
    const storedPhoto = newBaseFilesystemPath + tempFilename;
    console.log("storedPhoto " + storedPhoto);

    //convert to local http app
    const resolvedImg = this.webview.convertFileSrc(storedPhoto);
    //console.log("resolvedImg " + resolvedImg);

    //sanitize url string
    const safeImage = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(resolvedImg));
    console.log("safeImage " + safeImage);
    this.image = safeImage;
  }

  async recognizeImage(){

    const result = await this.worker.recognize(this.image);
    //console.log(result);
    this.ocrResult = result.data.text;
  }

  convertTextToSpeech() {

    this.textToSpeech.speak({
      text: this.ocrResult,
      locale: this.langSpeech,
      rate: this.speechRate
    })
  }

  stopSpeech(){

    this.textToSpeech.stop();
  }
}
